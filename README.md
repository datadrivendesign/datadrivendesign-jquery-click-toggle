# README #

### What is this repository for? ###

A simple plugin that adds a click event listener to all elements with the data attribute data-click-toggle. On click of the element with the attribute any class names that are set as the value of the attribute will be added to the element.

### How do I get set up? ###

NPM
```
#!bash

npm install -save git+ssh://git@bitbucket.org:datadrivendesign/datadrivendesign-jquery-click-toggle.git
```

Bower
```
#!bash

bower install -save https://bitbucket.org/datadrivendesign/datadrivendesign-jquery-click-toggle
```


### How do I use it ###

Add the data attribute data-toggle-click to any element. Set the value of the attribute to any classes you want added to the element on click. Thats it.

In the below example a bootstrap collapse anchor is hidden on click.

```
#!html

<a href="#collapse" data-toggle="collapse" data-click-toggle="hidden">READ MORE</a>
```