/**
 * Created by aronkerr on 11/12/16.
 */

/**
 * Adds event listener that checks for the data attribute data-click-toggle. By adding css class names as the value of
 * this attribute, they will be added to the element when it is clicked.
 */
(function ( $ ) {
    $('[data-click-toggle]').click(function() {
        var $el = $('[data-click-toggle]');
        $el.addClass($el.data('click-toggle'));
    });
}( jQuery ));